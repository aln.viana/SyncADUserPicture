<#
.SYNOPSIS
Written by Joakim at Jocha AB, http://jocha.se
Modified by Allan Viana, https://gitlab.com/aln.viana/

.DESCRIPTION
Version 1.4 - Updated 2018-06-26
This script downloads and sets the Active Directory profile photograph and sets it as your profile picture in Windows.
Attention: Remember to create a defaultuser.jpg in \\domain\netlogon\

Tested on Windows 10 Pro 1803 with the following tutorial:
https://newsignature.com/articles/ad-photo-synchronization-with-windows-10/

Some others, if the first don't work:
http://blog.jocha.se/tech/ad-user-pictures-in-windows-10 (From the original script writer)
https://www.codetwo.com/admins-blog/use-active-directory-user-photos-windows-10/

2018-06-28 : Now, this script only modify images if file modification date are less than user modification date.
2018-06-26 : Modified to better indicate errors.
2016-02-13 : Slightly adjusted.
2015-11-12 : Added all picture sizes for Windows 10 compatibility.
#>

[CmdletBinding(SupportsShouldProcess=$true)]Param()
function Test-Null($InputObject) { return !([bool]$InputObject) }

Try{
	#Get current user information.
	$user = ([ADSISearcher]"(&(objectCategory=User)(SAMAccountName=$env:username))").FindOne().Properties
	$user_sid = [System.Security.Principal.WindowsIdentity]::GetCurrent().User.Value
	Write-Output "Obtained current user information for $($user.displayname)..."
} Catch {
	Write-Error "Could not retrieve current user information." -ErrorAction Stop
}

Try {
	#Trying to get user photo from AD.
	$user_photo = $user.thumbnailphoto
	#Check if you have succeeded.
	If ((Test-Null $user_photo) -eq $false) {
		Write-Output "Photo exists in Active Directory."
	} Else {
		#If not, take default photo from AD.
		Write-Output "No photo found in Active Directory for $env:username, using the default image instead."
		$user_photo = [byte[]](Get-Content "\\$env:USERDNSDOMAIN\NETLOGON\defaultuser.jpg" -Encoding byte)
	}
} Catch {
	Write-Error "Could not get default photo from AD." -ErrorAction Stop
}

Try {
	#Set up image sizes and base path.
	$image_sizes = @(32, 40, 48, 96, 192, 200, 240, 448)
	$image_mask = "Image{0}.jpg"
	$image_base = $env:public + "\AccountPictures"
} Catch {
	Write-Error "Could not get Windows public folder." -ErrorAction Stop
}

Try {
	#Set up registry.
	$reg_base = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\AccountPicture\Users\{0}"
	$reg_key = [string]::format($reg_base, $user_sid)
	$reg_value_mask = "Image{0}"
	If ((Test-Path -Path $reg_key) -eq $false) { New-Item -Path $reg_key }
} Catch {
	Write-Error "Could not create user photo registry key." -ErrorAction Stop
}

Try { 
	#Create hidden directory, if it doesn't exist.
	$dir = $image_base + "\" + $user_sid
	If ((Test-Path -Path $dir) -eq $false) { $(mkdir $dir).Attributes = "Hidden" }
} Catch {
	Write-Error "Could not create photo folder." -ErrorAction Stop
}

#Save images, set reg keys.
ForEach ($size in $image_sizes) {
	Try {		
		$file_name = ([string]::format($image_mask, $size))
		$path = $dir + "\" + $file_name

		#Checks the date of user modification before modifying.
		if (![System.IO.File]::Exists($path) -or $user.whenchanged[0].Add((Get-TimeZone).BaseUtcOffset) -gt (Get-Item $path).LastWriteTime) {
			#Save photo to disk, overwrite existing files.
			Write-Output "  saving: $file_name"
			$user_photo | Set-Content -Path $path -Encoding Byte -Force
		}

		#Save the path in registry, overwrite existing entries.
		$name = [string]::format($reg_value_mask, $size)
		$value = New-ItemProperty -Path $reg_key -Name $name -Value $path -Force
	} Catch {
		Write-Error "Could not save file or registry key. Photo Size: $size"
	}
}

Write-Output "Done."